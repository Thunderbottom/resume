\ProvidesClass{mcdowellcvx}[2018/06/07 v.1.5 McDowell CV Class]


\ProcessOptions\relax

\LoadClass[letterpaper]{article} 

\usepackage[left=0.4in,top=0.1in,right=0.4in, bottom=0.1cm]{geometry} 


\usepackage[parfill]{parskip} 

\usepackage{array} 

\usepackage{etoolbox} 

\pagestyle{empty} 

\usepackage{tabu}

\usepackage{changepage}

\usepackage{enumitem}


\setlist{leftmargin=*, noitemsep, topsep=-1\parskip}

\usepackage{microtype}

\DeclareMicrotypeSet*{smallcapsi} { 
	encoding = {T1},
	shape    = {sc*,si,scit}
}


%--------------------------------------------------------------------------------
%                                    Constants                                  -
%--------------------------------------------------------------------------------
\def\afterheaderspace{4pt}
\def\beforesectionheaderspace{2pt}
\def\sectionheadermargin{4pt}
\def\aftersectionheaderspace{-4pt}
\def\sectionheaderhrlueheight{0.25pt}
\def\aftersinglelinesubsectionheaderspace{-18.5pt}
\def\afterdoublelinesubsectionheaderspace{-10pt}
\def\aftermultilinesubsectionheaderspace{-6pt}
\def\afteremptysubsectionheaderspace{3pt}
\def\subsectionmargin{9pt}
\def\aftersubsectionspace{2pt}

%--------------------------------------------------------------------------------
%                            Header Setup and Printing                          -
%--------------------------------------------------------------------------------

% Address
\makeatletter

\newcommand\address[1]{\def\@address{#1}}
\address{}

\newcommand\printaddress{
	\small{\@address}
}

% Webpage
\makeatletter

\newcommand\webpage[1]{\def\@webpage{#1}}
\address{}


\makeatother

% Name
\makeatletter

\newcommand\name[1]{\def\@name{#1}}
\name{}

\newcommand\printname{
	\textbf{\Large{\textsc{\@name}}}\linebreak
	\small{\@webpage}
}

\makeatother

% Contacts
\makeatletter

\newcommand\contacts[1]{\def\@contacts{#1}}
\contacts{}

\newcommand\printcontacts{
	\small{\@contacts}
}

\makeatother

% Github
\makeatletter

\newcommand\github[1]{\def\@github{#1}}
\contacts{}

\newcommand\printgithub{
	\small{\@github}
}

\makeatother

\makeatletter
\newcommand\makeheader{
	\begin{center}
		\begin{tabu} to 1\textwidth { X[l,m] X[2,c,m] X[r,m] }
			\printaddress & \printname & \printcontacts \printgithub \\
		\end{tabu}	
	\end{center}
	\vspace*{\afterheaderspace}
}
\makeatother

%--------------------------------------------------------------------------------
%                            Sections and Subsections                           -
%--------------------------------------------------------------------------------

% Print a section header
\makeatletter
\newenvironment{cvsection}[1]{
	\vspace*{\beforesectionheaderspace}
	% Set text margins to equal \tabcolsep (6pt by default)
	\begin{adjustwidth}{\sectionheadermargin}{\sectionheadermargin}
		\textsc{\textbf{#1}}
	\end{adjustwidth}
	\smallskip
	\hrule height \sectionheaderhrlueheight
	\vspace*{\aftersectionheaderspace}
}{}
\makeatother

% Print a subsection
\makeatletter
% Define toggles checking if titles were defined
\newtoggle{lefttitledefined}
\newtoggle{centertitledefined}
\newtoggle{righttitledefined}

\newenvironment{cvsubsection}[4][1]{
	\notblank{#2}{\toggletrue{lefttitledefined}}{}
	\notblank{#3}{\toggletrue{centertitledefined}}{}
	\notblank{#4}{\toggletrue{righttitledefined}}{}
	\ifboolexpr{togl {lefttitledefined} or togl {centertitledefined} or togl {righttitledefined}}{
		\begin{tabu} to 1\textwidth { X[l,p] X[c,p] X[r,p] }
			\textbf{#2} & \textbf{#3} & \textbf{#4} \\
		\end{tabu}
		% Add space according to the specidied number of lines
		\ifnumcomp{#1}{=}{1}{\vspace*{\aftersinglelinesubsectionheaderspace}}{
			\ifnumcomp{#1}{=}{2}{\vspace*{\afterdoublelinesubsectionheaderspace}}{
				\vspace*{\aftermultilinesubsectionheaderspace}
			}
		}
	}{
		\vspace*{\afteremptysubsectionheaderspace}
	}
	\togglefalse{lefttitledefined}
	\togglefalse{centertitledefined}
	\togglefalse{righttitledefined}
	\begin{adjustwidth}{\subsectionmargin}{\subsectionmargin}
}
{
	\end{adjustwidth}
	\vspace*{\aftersubsectionspace}
}
\makeatother
